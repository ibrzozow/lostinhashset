package pl.edu.pwr.lostinhashset.service.impl;

import java.util.Collections;
import java.util.List;

import pl.edu.pwr.lostinhashset.entity.User;
import pl.edu.pwr.lostinhashset.repository.UserRepository;
import pl.edu.pwr.lostinhashset.service.UserService;

public class DefaultUserService implements UserService {
	
	private UserRepository userRepository;

	@Override
	public List<User> getUsersSortedByName() {
		
		List<User> users = getUserRepository().getUsers();
//		users.sort(null);
		return users;
	}

	@Override
	public List<User> getUsersSortedByPoints() {

		List<User> users = getUserRepository().getUsers();
//		Collections.sort(users, null);
		return users;
	}

	public UserRepository getUserRepository() {
		return userRepository;
	}

	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

}
